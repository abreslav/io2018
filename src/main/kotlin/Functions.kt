sealed class Box<out T> {
    abstract val value: T
    abstract fun <R> map(map: (T) -> R): Box<R>

    companion object {
        val empty get(): Box<Nothing> = EmptyBox
        fun <T> full(t: T): Box<T> = FullBox(t)
    }
}

private class FullBox<T>(override val value: T) : Box<T>() {
    override fun <R> map(map: (T) -> R) = FullBox(map(value))
    override fun toString() = "$value in a Box"
}

private object EmptyBox: Box<Nothing>() {
    override val value: Nothing
        get() = throw IllegalStateException("No value in an empty box")

    override fun <R> map(map: (Nothing) -> R) = this
    override fun toString() = "Empty Box"
}

fun main(args: Array<String>) {
    fun upperCaseToString(o: Any): CharSequence = o.toString().toUpperCase()

    println(Box.full("abc").map(::upperCaseToString))
    println(Box.empty.map(::upperCaseToString))

}