package _05_coroutines;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Threads {
    public static void main(String[] args) {
        int n = 100_000;
        IntStream.range(0, n).mapToObj((i) -> {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(i);
                }
            };
            thread.start();
            return thread;
        }).collect(Collectors.toList()).forEach((thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }));
    }
}
