package _06_lazy_seq;

import java.util.Iterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class LazySequence {
    public static void main(String[] args) {
        Stream<Integer> stream = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        new Iterator<Integer>() {
                            int a = 1;
                            int b = 1;

                            @Override
                            public Integer next() {
                                int result = a;
                                a = b;
                                b += result;
                                return result;
                            }

                            @Override
                            public boolean hasNext() {
                                return true;
                            }
                        },
                        0
                ),
                false
        );
        System.out.println(
                stream.limit(20).collect(Collectors.toList())
        );
    }
}
