package _07_conventions

class Bag(vararg val items: Int)

fun main(args: Array<String>) {
    val bag = Bag(1, 2, 3)
//    for (i in bag) {
//        println(i)
//    }
}




















/*

operator fun Bag.iterator() = items.iterator()

operator fun Bag.plus(other: Bag) = Bag(*(items + other.items))

operator fun Bag.component1() = items[1]
operator fun Bag.component2() = Bag(*items.copyOfRange(1, items.size))

fun main(args: Array<String>) {
    val bag = Bag(1, 2, 3) + Bag(4)
    val (h, t) = bag
    for (i in t) {
        println(i)
    }
}

 */