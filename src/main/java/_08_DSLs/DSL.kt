package _08_DSLs

fun main(args: Array<String>) {
    val sb = StringBuilder()

    sb.append("[")
    for (i in 1..9) {
        sb.append(i)
        sb.append(", ")
    }
    sb.append(10)
    sb.append("]")

    println(sb)
}