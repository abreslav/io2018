package _02_properties.java

var observeMe = "a"
    set(new) {
        val old = field
        field = new
        println("$old -> $new")
    }



fun main(args: Array<String>) {
    println("\n\nObservable property:")
    observeMe = "bb"
    observeMe = "ccc"
    observeMe = "dddd"
}
