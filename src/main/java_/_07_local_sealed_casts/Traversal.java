package _07_local_sealed_casts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Traversal {

    public static void main(String[] args) {
        Container root = new Container(
                new Text("a"),
                new Container(
                        new Text("b"),
                        new Container(
                                new Text("c"),
                                new Text("d")
                        ),
                        new Text("e")
                ),
                new Text("f")
        );

        System.out.println(extractText(root));
    }

    public static String extractText(Element e) {
        return extractText(e, new StringBuilder()).toString();
    }

    private static StringBuilder extractText(Element e, StringBuilder sb) {
        if (e instanceof Text) {
            Text text = (Text) e;
            sb.append(text.getText());
        } else if (e instanceof Container) {
            Container container = (Container) e;
            for (Element child : container.getChildren()) {
                extractText(child, sb);
            }
        }
        return sb;
    }

    public interface Element {}

    public static class Container implements Element {
        private final List<Element> children;

        public Container(Element... children) {
            this.children = new ArrayList<>(Arrays.asList(children));
        }

        public List<? extends Element> getChildren() {
            return children;
        }
    }

    public static class Text implements Element {
        private final String text;

        public Text(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }
}
