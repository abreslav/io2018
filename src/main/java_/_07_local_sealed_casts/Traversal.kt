package _07_local_sealed_casts

sealed class Element

class Container(vararg children: Element) : Element() {
    val children = listOf(*children)
}
class Text(val text: String) : Element()

fun Element.extractText(): String {
    val sb = StringBuilder()
    fun doExtract(e: Element) {
        when (e) {
            is Text -> sb.append(e.text)
            is Container -> e.children.forEach(::doExtract)
        }
    }
    doExtract(this)
    return sb.toString()
}

fun main(args: Array<String>) {
    val root = Container(
            Text("a"),
            Container(
                    Text("b"),
                    Container(
                            Text("c"),
                            Text("d")
                    ),
                    Text("e")
            ),
            Text("f")
    )

    println(root.extractText())
}