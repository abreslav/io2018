package _06_delegated_properties;

public class ComplexProperties {
    private String os;

    public String getOs() {
        if (os == null) {
            System.out.println("Computing...");
            os = System.getProperty("os.name") +
                    " v" + System.getProperty("os.version") +
                    " (" + System.getProperty("os.arch") + ")";
        }
        return os;
    }

    private int observable = 0;
    
    public int getObservable() {
        return observable;
    }

    protected boolean beforeChange(int oldValue, int newValue) {
        System.out.println("About to change from " + oldValue + " to " + newValue);
        return oldValue < newValue;
    }
    
    protected void afterChange(int oldValue, int newValue) {
        System.out.println("Changed to " + newValue);
    }

    public void setObservable(int newValue) {
        int oldValue = this.observable;
        if (beforeChange(oldValue, newValue)) {
            this.observable = newValue;
            afterChange(oldValue, newValue);
        }
    }

    private String data = "";

    public String getData() {
        return data;
    }

    public void setData(String data) {
        if (data.length() > this.data.length()) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        ComplexProperties p = new ComplexProperties();
        for (int i = 0; i < 10; i++)
            System.out.println(p.getOs());

        p.setData("a");
        System.out.println(p.getData());
        p.setData("bb");
        System.out.println(p.getData());
        p.setData("ccc");
        System.out.println(p.getData());
        p.setData("d");
        System.out.println(p.getData());
        p.setObservable(1);
        p.setObservable(2);
        p.setObservable(1);
    }
}
