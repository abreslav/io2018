package _06_delegated_properties

import kotlin.properties.Delegates
import kotlin.properties.ObservableProperty
import kotlin.reflect.KProperty

val os by lazy {
    println("Computing...")
    System.getProperty("os.name") +
    " v" + System.getProperty("os.version") +
    " (" + System.getProperty("os.arch") + ")"
}

var observable: String by object : ObservableProperty<String>("") {
    override fun beforeChange(property: KProperty<*>, oldValue: String, newValue: String): Boolean {
        return newValue.length > oldValue.length
    }

    override fun afterChange(property: KProperty<*>, oldValue: String, newValue: String) {
        println("${property.name} changed from '$oldValue' to '$newValue'")
    }
}

var data: String by Delegates.vetoable("") {
    p, old, new ->
    new.length > old.length
}

fun main(args: Array<String>) {
    for (i in 0..9)
        println(os)

    data = "a"
    println(data)
    data = "bb"
    println(data)
    data = "ccc"
    println(data)
    data = "d"
    println(data)


    observable = "a"
    observable = "bb"
    observable = "ccc"
    observable = "dd"
}