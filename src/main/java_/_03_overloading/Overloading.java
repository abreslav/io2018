package _03_overloading;

import java.io.File;
import java.io.IOException;

public class Overloading {
    public static void main(String[] args) throws IOException {
        TempFileUtil.deleteAllOnExit(

                File.createTempFile("tmp", ".txt", new File(".")),

                File.createTempFile("tmp", ".txt"),

                File.createTempFile("tmp", null, new File(".")),

                File.createTempFile("tmp", null)
        );
    }
}
