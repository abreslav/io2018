package _03_overloading;

import java.io.File;
import java.io.IOException;

public class TempFileUtil {
    public static File createTempFile(String prefix, String suffix, File directory) throws IOException {
        return File.createTempFile(prefix, suffix, directory);
    }

    public static File createTempFile(String prefix, String suffix) throws IOException {
        return createTempFile(prefix, suffix, null);
    }

    public static File createTempFile(String prefix, File directory) throws IOException {
        return createTempFile(prefix, null, directory);
    }

    public static File createTempFile(File directory) throws IOException {
        return createTempFile("tmp", directory);
    }

    public static File createTempFile(String prefix) throws IOException {
        return createTempFile(prefix, null, null);
    }

    public static File createTempFile() throws IOException {
        return createTempFile("tmp");
    }

    public static File createTempFileWithSuffix(String suffix) throws IOException {
        return createTempFile("tmp", suffix);
    }

    public static File createTempFileWithSuffix(String suffix, File directory) throws IOException {
        return createTempFile("tmp", suffix, directory);
    }





    public static void main(String[] args) throws IOException {
        deleteAllOnExit(

                createTempFile(),
                createTempFile("tmp"),
                createTempFile(new File(".")),
                createTempFile("tmp", ".txt"),
                createTempFile("tmp", new File(".")),
                createTempFile("tmp", ".txt", new File(".")),
                createTempFileWithSuffix(".txt", new File(".")),
                createTempFileWithSuffix(".txt")
        );







        {
            File tmpFile = TempFileUtil.tempFile()
                    .prefix("tmp")
                    .suffix(".txt")
                    .directory(new File("."))
                    .build();
            System.out.println(tmpFile);
            tmpFile.deleteOnExit();
        }
    }



    public static void deleteAllOnExit(File... files) {
        for (File file : files) {
            System.out.println(file);
            file.deleteOnExit();
        }
    }



    public static class Builder {
        private String prefix = "tmp";
        private String suffix;

        private File directory;

        private Builder() {}

        public Builder prefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        public Builder suffix(String suffix) {
            this.suffix = suffix;
            return this;
        }

        public Builder directory(File directory) {
            this.directory = directory;
            return this;
        }
        public File build() throws IOException {
            return createTempFile(prefix, suffix, directory);
        }

    }

    public static Builder tempFile() {
        return new Builder();
    }
}
