package _03_overloading

import java.io.File

fun main(args: Array<String>) {
    val workdir = File(".")
    deleteAllOnExit(

            createTempFile("tmp", ".txt", workdir),

            createTempFile("tmp", ".txt"),

            createTempFile("tmp"),

            createTempFile("tmp", directory = workdir),

            createTempFile(suffix = ".txt"),

            createTempFile(directory = workdir),

            createTempFile(suffix = ".txt", directory = workdir),

            createTempFile()
    )
}


private fun deleteAllOnExit(vararg files: File) {
    files.forEach {
        println(it)
        it.deleteOnExit()
    }
}