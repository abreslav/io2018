import java.util.function.IntConsumer;

public class Repeat {
    public static void repeat(int times, IntConsumer body) {
        for (int i = 0; i < times; i++) {
            body.accept(i);
        }
    }

    public static void main(String[] args) {
        repeat(5, (i) -> {
            System.out.println(i);
        });



//        repeat(
//                5,
//                new IntConsumer() {
//                    @Override
//                    public void accept(int i) {
//                        System.out.println(i);
//                    }
//                }
//        );
    }
}
