package _02_extensions;

public class StringUtil {
    private StringUtil() {}

    public static String getFirstWord(String s) {
        int space = s.indexOf(' ');
        if (space < 0) return s;
        return s.substring(0, space);
    }

    public static void main(String[] args) {
        String fullName = "Jane Doe";

        System.out.println("First name: " + getFirstWord(fullName));
    }
}
