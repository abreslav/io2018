package _05_properties_and_classes;

import java.util.Objects;

public class ReturnClass {

    public static FullName parseFullName(String name) {
        int space = name.indexOf(' ');
        return new FullName(
                name.substring(0, space),
                name.substring(space + 1)
        );
    }




    public static void main(String[] args) {
        FullName jane = parseFullName("Jane Doe");
        System.out.println(jane);

        System.out.println("Jane's last name is " + jane.getLastName());

        System.out.println("Does equals() work? " + jane.equals(parseFullName("Jane Doe")));
    }

    public static class FullName {
        private final String firstName;
        private final String lastName;

        public FullName(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }











        @Override
        public String toString() {
            return "FullName(" +
                    "firstName=" + firstName +
                    ", lastName=" + lastName +
                    ')';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FullName fullName = (FullName) o;
            return Objects.equals(firstName, fullName.firstName) &&
                    Objects.equals(lastName, fullName.lastName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(firstName, lastName);
        }
    }

}
