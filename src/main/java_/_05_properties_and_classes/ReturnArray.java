package _05_properties_and_classes;

import java.util.Arrays;

public class ReturnArray {
    
    public static String[] parseName(String name) {
        int space = name.indexOf(' ');
        return new String[] {
                name.substring(0, space),
                name.substring(space + 1)
        };
    }




    public static void main(String[] args) {
        String[] name = parseName("Jane Doe");
        String first = name[0];
        String last = name[1];

        System.out.println(first + " " + last);

        System.out.println("Does equals() work? " + Arrays.equals(name, parseName("Jane Doe")));
    }
}
