package _05_properties_and_classes.java

fun parseFullName(name: String): FullName {
    val space = name.indexOf(' ')
    return FullName(
            name.substring(0, space),
            name.substring(space + 1)
    )
}

class FullName {
    val firstName: String
    val lastName: String

    constructor(firstName: String, lastName: String) {
        this.firstName = firstName
        this.lastName = lastName
    }
}

fun main(args: Array<String>) {
    val jane = parseFullName("Jane Doe")
    println(jane)

    println("Jane's last name is " + jane.lastName)

    println("Does equals() work? " + (jane == parseFullName("Jane Doe")))
}
