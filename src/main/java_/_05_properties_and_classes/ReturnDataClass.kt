package _05_properties_and_classes

data class FullName(val firstName: String, val lastName: String)

fun parseFullName(name: String): FullName {
    val space = name.indexOf(' ')
    return FullName(
            name.substring(0, space),
            name.substring(space + 1)
    )
}

fun main(args: Array<String>) {
    val jane = parseFullName("Jane Doe")
    println(jane)

    println("Jane's last name is " + jane.lastName)

    println("Does equals() work? " + (jane == parseFullName("Jane Doe")))

    val john = jane.copy(firstName = "John")
    println("Copy and change name: $john")
}