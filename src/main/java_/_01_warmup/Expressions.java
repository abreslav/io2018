package _01_warmup;

public class Expressions {
    public enum License {
        GPL, APACHE, MIT, BSD, PROPRIETARY
    }

    public static void ifStatement(License license) {
        String licenseType;
        if (license == License.PROPRIETARY) {
            System.out.println("Going proprietary");
            licenseType = "proprietary";
        } else {
            System.out.println("Yay, Open Source!");
            licenseType = "OSS";
        }

        System.out.println("The " + license + " license is " + licenseType);
    }


    public static void switchStatement(License license) {
        String licenseType;
        switch (license) {
            case PROPRIETARY:
                licenseType = "copyright";
                break;
            case GPL:
                licenseType = "copyleft";
                break;
            case APACHE:
            case MIT:
            case BSD:
                licenseType = "copycenter";
                break;
            default:
                throw new IllegalArgumentException("Unknown enum value: " + license);
        }
        System.out.println("The " + license + " license is " + licenseType);
    }
}
