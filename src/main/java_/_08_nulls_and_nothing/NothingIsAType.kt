package _08_nulls_and_nothing

fun main(args: Array<String>) {
    val a = args.getOrNull(0) ?: return
    val b = args.getOrNull(1) ?: err()
    println("a = '$a'")
    println("b = '$b'")

    var i = 0
    while (true) {
        val v = args.getOrNull(i) ?: break
        println("args[$i] = '$v'")
        i++
    }
}

private fun err(): Nothing = throw IllegalStateException("Call me with two arguments")