package _08_nulls_and_nothing

import java.util.*

private val RANDOM = Random()

private fun generateNewId(): String {
    return RANDOM.nextInt().toString() + ""
}

fun defaults(userId: String?) {
    val id = userId ?: generateNewId()
    println("Id: $id")
}

fun entryCheck(userId: String?) {
    if (userId == null) return

    println("User id: $userId")
}

fun assertion(userId: String?) {
    if (userId == null) throw IllegalArgumentException("Non-null user id expected")

    println("User id: $userId")
}

fun main(args: Array<String>) {

}