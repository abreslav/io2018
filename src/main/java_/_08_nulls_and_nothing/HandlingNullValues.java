package _08_nulls_and_nothing;

import java.util.Random;

public class HandlingNullValues {

    private static final Random RANDOM = new Random();

    private String generateNewId() {
        return RANDOM.nextInt() + "";
    }

    public void defaults(String userId) {
        String id = userId != null ? userId : generateNewId();
        System.out.println("Id: " + id);
    }

    public void entryCheck(String userId) {
        if (userId == null) return;

        System.out.println("User id: " + userId);
    }

    public void assertion(String userId) {
        if (userId == null) throw new IllegalArgumentException("Non-null user id expected");

        System.out.println("User id: " + userId);
    }
}
