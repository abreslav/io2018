fun testA(x: Any, y: Double) = x is Double && x == y

fun testB(x: Any, y: Double): Boolean {
    if (x !is Double) return false
    val tmp: Double = x
    return tmp == y
}

fun example1(x: Comparable<Double>, y: Double) =
        x is Double && x < y

fun example1a(x: Comparable<Double>, y: Double): Boolean {
    if (x !is Double) return false
    val tmp: Double = x
    return tmp < y
}

fun example2(x: Any, y: Any) =
        x is Int && y is Double && x < y // total order comparison

fun example2a(x: Int, y: Double) =
        x < y // widening conversion for 'x' + IEEE 754 comparison

fun example3(x: Any) =
        when (x) {
            !is Double -> "!Double"
            0.0 -> "0.0" // (*)
            else -> "other"
        }

fun example3a(x: Any) =
        if (x !is Double) "!Double"
        else if (x == 0.0) "0.0"
        else "other"

fun main(args: Array<String>) {
    println(testA(0.0, -0.0))
    println(testB(0.0, -0.0))
    println(example1(0.0, -0.0))
    println(example1a(0.0, -0.0))
    println(example2(0, -0.0))
    println(example2a(0, -0.0))
    println(example3(-0.0))
    println(example3a(-0.0))
}