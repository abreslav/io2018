package com.example;

import java.util.function.Function;

abstract class Box<T> {
    private Box() {}

    public static <T> Box<T> empty() {
        return new EmptyBox<>();
    }

    public static <T> Box<T> full(T t) {
        return new FullBox<>(t);
    }

    public abstract T getValue();

    public abstract <R> Box<R> map(Function<T, R> map);

    private static class FullBox<T> extends Box<T> {

        private final T value;

        public FullBox(T value) {
            this.value = value;
        }

        @Override
        public T getValue() {
            return value;
        }

        @Override
        public <R> Box<R> map(Function<T, R> map) {
            return new FullBox<>(map.apply(getValue()));
        }

        @Override
        public String toString() {
            return getValue() + " in a Box";
        }
    }

    private static class EmptyBox<T> extends Box<T> {
        @Override
        public T getValue() {
            throw new IllegalStateException("No value in an empty box");
        }

        @Override
        public <R> Box<R> map(Function<T, R> map) {
            return new EmptyBox<>();
        }

        @Override
        public String toString() {
            return "Empty Box";
        }
    }
}

public class Functions {

    private static final Function<Object, CharSequence> UCTS = Functions::upperCaseToString;

    public static CharSequence upperCaseToString(Object o) {
        return o.toString().toUpperCase();
    }

    public static void main(String[] args) {
        System.out.println(Box.full("abc").map(Functions::upperCaseToString));
        System.out.println(Box.empty().map(Functions::upperCaseToString));
    }

}